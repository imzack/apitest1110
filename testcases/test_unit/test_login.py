import pytest
from common.myfiles import FileHandler
from common.myrequests import MyRequests

myreq = MyRequests()
fh = FileHandler()
testdatajson = fh.read_json('login.json')
testdatacsv = fh.read_csv('login.csv')
testdataexcel = fh.read_excel('login.xlsx')
class TestFileDDTLogin:


    @pytest.mark.parametrize('phone,passwd,expect_resultcode,expect_message',testdatajson)
    def test_login_json(self,phone,passwd,expect_resultcode,expect_message):
        url = "http://49.233.108.117:28019/api/v1/user/login"
        postdata = {
            "loginName": phone,
            "passwordMd5": passwd
        }
        # r = requests.post(url=url, json=postdata)
        r = myreq.do_requests(method='post',url=url,json=postdata)
        print(r.json())
        assert r.json()['resultCode'] == expect_resultcode
        assert r.json()["message"] == expect_message

    @pytest.mark.parametrize('phone,passwd,expect_resultcode,expect_message', testdatacsv)
    def test_login_csv(self, phone, passwd, expect_resultcode, expect_message):
        url = "http://49.233.108.117:28019/api/v1/user/login"
        postdata = {
            "loginName": phone,
            "passwordMd5": passwd
        }
        # r = requests.post(url=url, json=postdata)
        r = myreq.do_requests(method='post', url=url, json=postdata)
        print(r.json())
        # csv 文件中读取的数据，数字会被转换为字符串，这里将字符串转换数字
        assert r.json()['resultCode'] == int(expect_resultcode)
        assert r.json()["message"] == expect_message

    @pytest.mark.parametrize('phone,passwd,expect_resultcode,expect_message', testdataexcel)
    def test_login_excel(self, phone, passwd, expect_resultcode, expect_message):
        url = "http://49.233.108.117:28019/api/v1/user/login"
        postdata = {
            "loginName": phone,
            "passwordMd5": passwd
        }
        # r = requests.post(url=url, json=postdata)
        r = myreq.do_requests(method='post', url=url, json=postdata)
        print(r.json())
        # csv 文件中读取的数据，数字会被转换为字符串，这里将字符串转换数字
        assert r.json()['resultCode'] == int(expect_resultcode)
        assert r.json()["message"] == expect_message