"""
测试订单流程
前提条件：已经成功注册好账号
流程：
1. 登录 --> token
2. 搜索 --> 商品id
3. 加入购物车 --> 购物车id
4. 生成订单 --> 订单id
5. 结算订单
"""

from common.myrequests import MyRequests
from faker import Faker
import pytest
import json
from common.myutils import get_root_dir
import os
import jsonschema
from common.mylogger import logger


# 定义login 登录schema文件的路径
loginschema_file = os.path.join(get_root_dir(),'testdata/schema/login.json')
with open(loginschema_file,mode='r',encoding='utf8') as file:
    login_schame = json.load(file)

searchschema_file = os.path.join(get_root_dir(),'testdata/schema/search.json')
with open(searchschema_file,mode='r',encoding='utf8') as file:
    search_schame = json.load(file)

myreq = MyRequests()
# 设置地区
fk = Faker(['zh_CN'])

class TestData:
    token = None
    # 生成随机的手机号
    phone = fk.phone_number()
    # 手机密码
    passwd = "123456"
    # goodsid
    goodsid = ""
    # 地址ID
    addressid = ""

    cartitems = ""

    orderid = ""

# 运行之前先注册好账号
@pytest.fixture(scope='module',autouse=True)
def register_user():

    url = "http://49.233.108.117:28019/api/v1/user/register"
    data = {
        "loginName": TestData.phone,
        "password": TestData.passwd
    }
    res = myreq.do_requests(method='post',json=data,url=url,bussiness="注册账号")
    assert res.status_code == 200
    assert res.json()["resultCode"] == 200
    assert res.json()['message'] == "SUCCESS"
    # 注册成功之后将 phone 返回出来
    yield

# 定义测试类
class TestOrder:

    def test_login(self):
        url = "http://49.233.108.117:28019/api/v1/user/login"
        import hashlib
        passwdmd5 = hashlib.md5(TestData.passwd.encode())
        data = {
            "loginName": TestData.phone,
            "passwordMd5": passwdmd5.hexdigest()
        }
        res = myreq.do_requests(method='post',url=url,json=data,bussiness='登录用户')
        # 针对schema 进行验证
        try:
            jsonschema.validate(res.json(),login_schame)
            assert res.status_code == 200
            assert res.json()["resultCode"] == 200
            assert res.json()['message'] == "SUCCESS"
            # 上下游传参
            # 赋值给类变量
            TestData.token = res.json()['data']
        except Exception as e:
            logger.error(e)
            raise e



    def test_search(self):
        url = 'http://49.233.108.117:28019/api/v1/search'
        header = {
            # 调用类变量
            "token": TestData.token
        }
        query = {"keyword": "Iphone"}
        # res = requests.get(url, headers=header, params=query)
        res = myreq.do_requests(method='get',url=url, param=query,headers =header,bussiness='搜索业务')

        jsonschema.validate(res.json(),search_schame)

        assert res.status_code == 200
        assert res.json()["resultCode"] == 200
        assert res.json()['message'] == "SUCCESS"
        for data in res.json()["data"]["list"]:
            # 每条搜索结果中包含 iPhone 关键字
            assert 'IPhone'.upper() in data["goodsName"].upper()

        TestData.goodsid = res.json()['data']["list"][0]["goodsId"]


    def test_add_carts(self):
        url = "http://49.233.108.117:28019/api/v1/shop-cart"
        bodydata = {
          "goodsCount": 1,
          "goodsId": TestData.goodsid
        }
        header = {
            # 调用类变量
            "token": TestData.token
        }
        res = myreq.do_requests(method='Post',url=url,json=bodydata,headers = header)
        assert res.json()["resultCode"] == 200
        assert res.json()["message"] == "SUCCESS"
    def test_add_address(self):
        """
        添加用户地址
        :return:
        """
        url = "http://49.233.108.117:28019/api/v1/address"
        header = {
            # 调用类变量
            "token": TestData.token
        }
        bodydata = {
          "cityName": fk.city_name(),
          "defaultFlag": 0,
          "detailAddress": fk.address(),
          "provinceName": fk.province(),
          "regionName": fk.street_address(),
          "userName": "helloworld",
          "userPhone": TestData.phone
        }
        res = myreq.do_requests(method='post',url=url,json=bodydata,headers = header)
    def test_get_my_cartitems(self):
        """
        查看购物车
        """
        url="http://49.233.108.117:28019/api/v1/shop-cart"
        header = {
            # 调用类变量
            "token": TestData.token
        }
        res = myreq.do_requests(method='get',url=url,headers = header)

        TestData.cartitems = res.json()["data"][0]["cartItemId"]

    def test_get_my_address(self):
        """
        查看我的收货地址
        :return:
        """
        url = 'http://49.233.108.117:28019/api/v1/address'
        header = {
            # 调用类变量
            "token": TestData.token
        }
        res = myreq.do_requests(method='get',url=url,headers = header)

        TestData.addressid = res.json()['data'][0]["addressId"]

    def test_order(self):
        """
        下订单
        :return:
        """
        header = {
            # 调用类变量
            "token": TestData.token
        }
        bodydata= {
          "addressId": TestData.addressid,
          "cartItemIds": [
            TestData.cartitems
          ]
        }

        url = "http://49.233.108.117:28019/api/v1/saveOrder"
        res = myreq.do_requests(method='post',url=url,json=bodydata,headers = header)
        TestData.orderid = res.json()["data"]

    def test_payment(self):
        """
        支付
        :return:
        """
        url = "http://49.233.108.117:28019/api/v1/paySuccess"
        header = {
            # 调用类变量
            "token": TestData.token
        }
        params = {
            "orderNo":TestData.orderid,
            "payType":0
        }

        res = myreq.do_requests(method='get',url=url,param=params)
        assert res.json()["resultCode"] == 200