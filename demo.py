# 导入模块
import unittest

# 单元测试类必须继承 unittest.TestCase 类
class TestA(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print('执行类之前运行...')

    @classmethod
    def tearDownClass(cls) -> None:
        print('类中的用例执行完成之后....')


    def setUp(self) -> None:
        print('方法执行之前')


    def tearDown(self) -> None:
        print('方法运行之后')


    def test_1login(self):
        print('登录。。。')

    def test_2add_cart(self):
        print('添加购物车...')

    def test_3order(self):
        print('下订单....')

    def test_4payment(self):
        print('支付...')

if __name__ == '__main__':
    # 运行测试用例
    unittest.main(verbosity=2)