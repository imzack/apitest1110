import unittest
from ddt import ddt,data,unpack

@ddt
class TestLogin(unittest.TestCase):
    @data(1,2,3,4)
    def test_login(self,val):
        """
        生成四个测试用例
        :param val:
        :return:
        """
        print(f'开始登录。{val}')

    @data(("17712341234","123456"),("","123456"),("17712341234",""),("",""))
    @unpack
    def test_login_username_passwd(self,username,passwd):
        """
        传入4组数据,每组数据表示用户名和密码
        :param username: 用户名
        :param passwd:   密码
        :return:
        """
        print(f'使用用户名：{username},密码：{passwd}进行登录')




if __name__ == '__main__':
    # 执行文件中unittest
    unittest.main(verbosity=2)