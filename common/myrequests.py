import requests
from common.mylogger import logger
class MyRequests:

    def do_requests(self,method:str,url,param=None,data=None,json=None,bussiness=None, *args,**kwargs):
        """
        自定义的请求处理方法
        :param method: 请求方法  GET，POST
        :param url: 请求路径
        :param param: get请求参数
        :param data: post请求参数
        :param json: post 请求json格式数据
        :param args:
        :param kwargs:
        :return:
        """
        if method.lower() =="get":
            logger.debug(f'业务：{bussiness} 发送get请求,请求地址：{url},请求参数:{param}')
            try:
                r = requests.get(url, params=param, *args, **kwargs)
                logger.debug(f'服务器返回结果:{r.status_code} {r.text}')
                return r
            except Exception:
                logger.error(msg=Exception, exc_info=True)
                raise Exception
        elif method.lower() == "post":
            logger.debug(f'业务：{bussiness} 发送post请求,请求地址：{url},请求参数:{data}, {json}')
            try:
                r = requests.post(url, data=data, json=json, *args, **kwargs)
                logger.debug(f'服务器返回结果:{r.status_code} {r.text}')
                return r
            except Exception:
                logger.error(msg=Exception,exc_info=True)
                raise Exception




if __name__ == '__main__':
    my = MyRequests()
    my.do_requests(method='get',url='http://49.233.108.117:28019/api/v1/index-infos')