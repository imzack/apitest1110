"""
处理文件
"""
from common.myutils import get_root_dir
import os
import json
import csv
from openpyxl import load_workbook

class FileHandler:
    def __init__(self):
        # 定义存放测试数据的目录，所有的测试数据都放在此目录下
        self.data_dir = os.path.join(get_root_dir(),'testdata/unitdata')


    def read_json(self,jsonfile):
        """
        读取json数据文件，返回参数化的数据
        :param jsonfile: json文件的相对路径
        :return:
        """
        file = os.path.join(self.data_dir,jsonfile)
        # 读取json文件的内容
        testdata = []
        with open(file,mode="r",encoding='utf8') as f:
            # 加载文件
            jsondata = json.load(f)
            for key,value in jsondata.items():
                # print(key,value)
                v_data = []
                for k,v in value.items():
                    v_data.append(v)
                # print(v_data)
                testdata.append(tuple(v_data))

        # print(testdata)
        return testdata


    def read_csv(self,csvfile):
        testdata= []
        file = os.path.join(self.data_dir,csvfile)
        with open(file,mode='r',encoding='utf8') as f:
            csvdata = csv.reader(f)
            for line in csvdata:
                testdata.append(tuple(line))
        return testdata


    def read_excel(self,excelfile):
        testdata = []
        file = os.path.join(self.data_dir,excelfile)
        # 拿到整个工作表
        wb=load_workbook(filename=file)
        # 获取默认的worksheet
        ws = wb.active
        # 从第一行，第一列开始取值
        for row in ws.iter_rows(min_row=1,max_row=ws.max_row,min_col=1,max_col=ws.max_column,values_only=True):
            testdata.append(row)

        return testdata


if __name__ == '__main__':
    # 实例化类
    fh = FileHandler()
    # print(fh.read_json('login.json'))
    # print(fh.read_csv('login.csv'))
    fh.read_excel('login.xlsx')