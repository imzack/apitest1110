import logging

# 设置名字
logger = logging.getLogger("apitesting")
# 设置级别
logger.setLevel(logging.DEBUG)

# 配置格式化样式  文本内容格式
formater = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s: %(message)s')

# 添加控制台处理器
ls = logging.StreamHandler()
ls.setLevel(logging.DEBUG)
# 设置格式
ls.setFormatter(formater)
# 控制台处理器添加到logger中
logger.addHandler(ls)

# 添加文件处理器
import os
import time
logsdir = os.path.join( os.path.dirname( os.path.dirname(__file__)),'logs')
# 日志目录定义
if not os.path.exists(logsdir):
    os.mkdir(logsdir)
# 日志文件 以当时运行的日志格式
logfile = os.path.join(logsdir,time.strftime('%Y_%m_%d')+'.log')
lf = logging.FileHandler(filename=logfile,encoding='utf8')
lf.setLevel(logging.DEBUG)
lf.setFormatter(formater)
# 文件处理器添加到日志当中
logger.addHandler(lf)

# logger.debug('this is debug')
