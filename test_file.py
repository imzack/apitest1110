import unittest
from ddt import ddt,file_data
from common.myutils import get_root_dir
import os
@ddt
class TestData(unittest.TestCase):
    @file_data(os.path.join(get_root_dir(),'testdata/unitdata/login.yaml'))
    def test_file(self,loginname,passwd,resultcode,message):
        print(f'使用用户名{loginname},密码{passwd} 进行登录')
        print(f'期望结果的状态码为{resultcode}，期望结果为{message}')


if __name__ == '__main__':
    unittest.main(verbosity=2)